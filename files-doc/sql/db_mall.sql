/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 8.0.21 : Database - db_mall
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_mall` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `db_mall`;

/*Table structure for table `t_account` */

DROP TABLE IF EXISTS `t_account`;

CREATE TABLE `t_account` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` bigint DEFAULT NULL,
  `money` int DEFAULT '0',
  `is_deleted` tinyint DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `t_account` */

insert  into `t_account`(`id`,`user_id`,`money`,`is_deleted`) values 
(1,1001,300000,0),
(2,1002,200000,0);

/*Table structure for table `t_cart` */

DROP TABLE IF EXISTS `t_cart`;

CREATE TABLE `t_cart` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `cart_key` varchar(255) NOT NULL DEFAULT '255' COMMENT '密钥',
  `user_id` bigint DEFAULT '20' COMMENT '用户编号',
  `yn` tinyint DEFAULT NULL COMMENT '是否有效 1:有效 0:无效',
  `sale_type` tinyint DEFAULT '1' COMMENT '销售模式1:控销 2:流通',
  `version` int DEFAULT '1' COMMENT '修改版本',
  `is_deleted` tinyint DEFAULT '0',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8408 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='购物车';

/*Data for the table `t_cart` */

insert  into `t_cart`(`id`,`cart_key`,`user_id`,`yn`,`sale_type`,`version`,`is_deleted`,`create_time`,`update_time`) values 
(8000,'aa',100000,1,1,1,0,'2021-07-02 16:23:28','2021-07-02 16:23:38'),
(8405,'initKey',100010,1,1,10,0,'2021-07-02 16:24:29','2021-07-02 17:29:08');

/*Table structure for table `t_order` */

DROP TABLE IF EXISTS `t_order`;

CREATE TABLE `t_order` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` bigint DEFAULT NULL,
  `goods_code` varchar(40) DEFAULT NULL,
  `count` int DEFAULT '0',
  `money` int DEFAULT '0',
  `is_deleted` tinyint DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8;

/*Data for the table `t_order` */

/*Table structure for table `t_storage` */

DROP TABLE IF EXISTS `t_storage`;

CREATE TABLE `t_storage` (
  `id` int NOT NULL AUTO_INCREMENT,
  `goods_code` varchar(40) DEFAULT NULL,
  `goods_name` varchar(200) DEFAULT NULL,
  `count` int DEFAULT '0',
  `is_deleted` tinyint DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `goods_code` (`goods_code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `t_storage` */

insert  into `t_storage`(`id`,`goods_code`,`goods_name`,`count`,`is_deleted`) values 
(1,'200491','星巴克咖啡杯',500,0);


/*Table structure for table `undo_log` */

DROP TABLE IF EXISTS `undo_log`;

CREATE TABLE `undo_log` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `branch_id` bigint NOT NULL,
  `xid` varchar(100) NOT NULL,
  `context` varchar(128) NOT NULL,
  `rollback_info` longblob NOT NULL,
  `log_status` int NOT NULL,
  `log_created` datetime NOT NULL,
  `log_modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_undo_log` (`xid`,`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=155 DEFAULT CHARSET=utf8;

/*Data for the table `undo_log` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
