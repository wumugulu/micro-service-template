
#### initMallData
> http://localhost:7012/main/initMallData
```
post json body
{
    "id": 333,
    "key": "县城",
    "sign": 992
}
```


#### testComplexData
> http://localhost:7012/main/testComplexData
```
post json body
{
    "id": 333,
    "key": "县城",
    "sign": 992
}
```


#### testRedissonLock
> http://localhost:7032/main/testRedissonLock
```
post json body
{
    "id": 8000,
    "key": "线程1"
}
```


#### testTransaction
> http://localhost:7032/main/testTransaction?formA=abc&formB=123
```
formdata:
?formA=abc&formB=123

post json body
{
    "userId": 1002,
    "goodsCode": "200491",
    "goodsCount": 1,
    "goodsPrice": 1000
}
```

