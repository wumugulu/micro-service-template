package com.wumugulu.dubbo.provider.account.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wumugulu.dubbo.basedata.entity.account.AccountEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 黑山老妖
 * @since 2021-06-22
 */
@Mapper
public interface AccountMapper extends BaseMapper<AccountEntity> {

    public int offsetMoneyById(@Param("id")Integer id, @Param("amount") Long amount);

}
