package com.wumugulu.dubbo.provider.account;

//import com.wumugulu.zkcenter.config.annotation.EnableZkCenter;
import com.yomahub.tlog.core.enhance.bytes.AspectLogEnhance;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableDubbo
//@EnableZkCenter
public class AccountProviderApp {

    static { AspectLogEnhance.enhance(); }

    public static void main(String[] args) {
        SpringApplication.run(AccountProviderApp.class, args);
    }

}
