package com.wumugulu.dubbo.provider.account.service.impl;

import cn.hutool.core.map.MapUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wumugulu.dubbo.basedata.entity.account.AccountEntity;
import com.wumugulu.dubbo.provider.universal.mybatisplus.MybatisPlusConfig;
import com.wumugulu.dubbo.provider.universal.mybatisplus.MybatisPlusPageUtil;
import com.wumugulu.dubbo.provider.account.mapper.AccountMapper;
import com.wumugulu.dubbo.provider.account.service.AccountService;
import io.seata.core.context.RootContext;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 黑山老妖
 * @since 2021-06-22
 */
@Service
@DubboService
public class AccountServiceImpl extends ServiceImpl<AccountMapper, AccountEntity> implements AccountService {

    @Override
    public boolean debtByUserId(Integer userId, Integer money) {
        System.err.println("事务，XID = " + RootContext.getXID());
        LambdaQueryWrapper<AccountEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(AccountEntity::getUserId, userId);
        AccountEntity accountOld = getOne(wrapper);

        AccountEntity accountNew = new AccountEntity();
        accountNew.setId(accountOld.getId()).setMoney(accountOld.getMoney() - money);
        return updateById(accountNew);
    }

    @Override
    public List<AccountEntity> queryList(Map<String, Object> paraMap) {
        Wrapper<AccountEntity> wrapper = null;
        if (paraMap != null)
            wrapper = getWrapper(paraMap);

        List<AccountEntity> entityList = list(wrapper);// baseMapper.selectList(wrapper);
        return entityList;
    }

    @Override
    public IPage<AccountEntity> queryPage(Map<String, Object> paraMap) {
        Wrapper<AccountEntity> wrapper = getWrapper(paraMap);
        Page<AccountEntity> page = MybatisPlusPageUtil.getPage(paraMap);

        IPage<AccountEntity> entityPage = page(page, wrapper);// baseMapper.selectPage(page, wrapper);
        return entityPage;
    }

    @Override
    public Integer offsetMoneyById(Integer id, Long amount) {
        return baseMapper.offsetMoneyById(id, amount);
    }

    private Wrapper<AccountEntity> getWrapper(Map<String, Object> paramMap) {
        Map<String, Object> kvMap = new HashMap<>();
        kvMap.putAll(paramMap);

        LambdaQueryWrapper<AccountEntity> wrapper = new LambdaQueryWrapper<>();

        Long userId = MapUtil.getLong(kvMap,"userId");
        wrapper.eq(userId != null, AccountEntity::getUserId, userId);

        Integer amount = MapUtil.getInt(kvMap, "amount");
        wrapper.eq(amount != null, AccountEntity::getMoney, amount);

        //设置动态表名的后缀!!!
        String tableId = MapUtil.getStr(kvMap, "tableId");
        if (tableId != null) { MybatisPlusConfig.TABLE_NAME_SUFFIX.set(tableId); }

        wrapper.orderByDesc(AccountEntity::getId);
        return wrapper;
    }

}
