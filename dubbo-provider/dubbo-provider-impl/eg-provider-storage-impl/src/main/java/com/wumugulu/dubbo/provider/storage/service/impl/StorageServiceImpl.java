package com.wumugulu.dubbo.provider.storage.service.impl;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wumugulu.dubbo.provider.universal.mybatisplus.MybatisPlusConfig;
import com.wumugulu.dubbo.provider.universal.mybatisplus.MybatisPlusPageUtil;
import com.wumugulu.dubbo.basedata.entity.storage.StorageEntity;
import com.wumugulu.dubbo.provider.storage.mapper.StorageMapper;
import com.wumugulu.dubbo.provider.storage.service.StorageService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 黑山老妖
 * @since 2021-06-22
 */
@Service
@DubboService
public class StorageServiceImpl extends ServiceImpl<StorageMapper, StorageEntity> implements StorageService {

    @Override
    public boolean reduceByGoodsCode(String goodsCode, Integer goodsCount) {
            LambdaQueryWrapper<StorageEntity> storageWrapper = new LambdaQueryWrapper<>();
            storageWrapper.eq(StorageEntity::getGoodsCode, goodsCode);
            StorageEntity storageOld = getOne(storageWrapper);

            StorageEntity storageNew = new StorageEntity();
            storageNew.setId(storageOld.getId()).setCount(storageOld.getCount()-goodsCount);
            return updateById(storageNew);
    }

    @Override
    public List<StorageEntity> queryList(Map<String, Object> paraMap) {
        Wrapper<StorageEntity> wrapper = getWrapper(paraMap);

        List<StorageEntity> entityList = list(wrapper);// baseMapper.selectList(wrapper);
        return entityList;
    }

    @Override
    public IPage<StorageEntity> queryPage(Map<String, Object> paraMap) {
        Page<StorageEntity> page = MybatisPlusPageUtil.getPage(paraMap);
        Wrapper<StorageEntity> wrapper = getWrapper(paraMap);

        IPage<StorageEntity> entityPage = page(page, wrapper);// baseMapper.selectPage(page, wrapper);
        return entityPage;
    }

    private Wrapper<StorageEntity> getWrapper(Map<String, Object> paramMap) {
        Map<String, Object> kvMap = new HashMap<>();
        kvMap.putAll(paramMap);

        LambdaQueryWrapper<StorageEntity> wrapper = new LambdaQueryWrapper<>();

        String name = MapUtil.getStr(kvMap, "name");
        wrapper.eq(StrUtil.isNotBlank(name), StorageEntity::getGoodsName, name);

        String commodityCode = MapUtil.getStr(kvMap, "commodityCode");
        wrapper.likeLeft(StrUtil.isNotBlank(commodityCode), StorageEntity::getGoodsCode, commodityCode);

        //设置动态表名的后缀!!!
        String tableId = MapUtil.getStr(kvMap, "tableId");
        if (tableId != null) { MybatisPlusConfig.TABLE_NAME_SUFFIX.set(tableId); }

        wrapper.orderByDesc(StorageEntity::getId);
        return wrapper;
    }

}
