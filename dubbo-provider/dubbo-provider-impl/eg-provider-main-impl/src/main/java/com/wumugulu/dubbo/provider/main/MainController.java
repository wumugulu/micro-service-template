package com.wumugulu.dubbo.provider.main;

import com.wumugulu.dubbo.provider.main.service.MainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

    @Autowired
    @Qualifier("DemoServiceBean")
    private MainService mainService;

    @RequestMapping("/mainProvider/sayHello")
    public String sayHello(String name) {
        return mainService.sayHello(name);
    }

}
