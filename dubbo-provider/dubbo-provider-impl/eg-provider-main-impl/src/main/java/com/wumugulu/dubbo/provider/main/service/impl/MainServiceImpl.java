package com.wumugulu.dubbo.provider.main.service.impl;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RandomUtil;
import com.wumugulu.dubbo.provider.account.service.AccountService;
import com.wumugulu.dubbo.basedata.entity.cart.CartEntity;
import com.wumugulu.dubbo.provider.cart.service.CartService;
import com.wumugulu.dubbo.provider.main.service.MainService;
import com.wumugulu.dubbo.basedata.entity.order.OrderEntity;
import com.wumugulu.dubbo.provider.order.service.OrderService;
import com.wumugulu.dubbo.provider.storage.service.StorageService;
import io.seata.core.context.RootContext;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.config.annotation.DubboService;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
@DubboService
@Slf4j
public class MainServiceImpl implements MainService {

    private final String LOCK_NAME = "DISTRIBUTE_LOCK";

    @Value("${spring.application.name}")
    private String appName;

    @Resource
    private RedissonClient redisson;

    @DubboReference
    private AccountService accountService;
    @DubboReference
    private StorageService storageService;
    @DubboReference
    private OrderService orderService;
    @DubboReference
    private CartService cartService;

    @Override
    public String sayHello(String name) {
        log.info("provider.main.sayHello: name={}", name);
        return "hello [" + name + "], how r u !";
    }

    /**
     * init mall data
     *
     * @param paraMap
     * @return
     */
    @Override
    public boolean initMallData(Map<String, Object> paraMap) {
        Integer id = 8000;
        CartEntity cartEntity = new CartEntity().setId(id).setCartKey(RandomUtil.randomString(10));
        return cartService.updateById(cartEntity);
//        return cartService.removeById(id);
    }

    /**
     * fetch complex data from mall
     *
     * @param paraMap
     * @return
     */
    @Override
    public Map<String, Object> testComplexData(Map<String, Object> paraMap) {
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("randomId", IdUtil.simpleUUID());
        resultMap.put("appName", appName);

        resultMap.put("account", accountService.queryPage(paraMap).getRecords());
        resultMap.put("storage", storageService.queryPage(paraMap).getRecords());
        resultMap.put("order", orderService.queryPage(paraMap).getRecords());
        resultMap.put("cart", cartService.queryPage(paraMap).getRecords());

        resultMap.put("countCartBySaleType", cartService.countCartBySaleType(1));
        resultMap.put("countAnyTable", cartService.countAnyTable("t_order"));

        return resultMap;
    }

    /**
     * 使用redisson实现《分布式锁》
     *
     * @param paraMap
     * @return
     */
    @Override
    public Map<String, Object> testRedissonLock(Map<String, Object> paraMap) {
        Map<String, Object> kvMap = new HashMap<>();
        kvMap.putAll(paraMap);
        Integer id = (Integer) kvMap.getOrDefault("id", 7000);

        //String userId = kvMap.getStr("user");

        String randomStr = RandomUtil.randomString(6);
        /* 获取锁 */
        RLock rLock = redisson.getLock(LOCK_NAME);

        boolean result = false;
        for (int i = 0; i < 100; i++) {
            try{

                /* 上锁 */
                rLock.lock();
                log.info("{} redisson locked, start ...", randomStr);

                CartEntity cartEntity = cartService.getById(id);
                Thread.sleep(10);
                CartEntity cartEntityUpdate = new CartEntity().setId(id).setUserId(cartEntity.getUserId()+1).setUpdateTime(new Date());
                result = cartService.updateById(cartEntityUpdate);


            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                log.info("{} redisson unlocked, finish !!!", randomStr);
                /* 解锁 */
                rLock.unlock();
            }
        }

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.putAll(kvMap);
        resultMap.put("result", result);
        return resultMap;
    }



    /**
     * 使用seata实现《分布式事务》
     *
     * @param paraMap
     * @return
     */
    @Override
    @GlobalTransactional(name = "dubbo-gts-seata-example", timeoutMills = 30000)
    public Map<String, Object> testTransaction(Map<String, Object> paraMap) {
        Map<String, Object> kvRequest = new HashMap<>();
        kvRequest.putAll(paraMap);

        Integer cartId = 8405;
        Integer userId = (Integer) kvRequest.getOrDefault("userId", 1002);
        String goodsCode = (String) kvRequest.getOrDefault("goodsCode", "200491");
        Integer goodsCount = (Integer) kvRequest.getOrDefault("goodsCount", 2);
        Integer goodsPrice = (Integer) kvRequest.getOrDefault("goodsPrice", 1000);

        String randomStr = RandomUtil.randomString(6);
        Map<String, Object> kvResult = new HashMap<>();
        kvResult.putAll(paraMap);
//        try{
            log.info("{} transaction start ...", randomStr);
            System.err.println("开始全局事务，XID = " + RootContext.getXID());

            boolean accountResult = accountService.debtByUserId(userId, goodsPrice * goodsCount);
            kvResult.put("accountResult", accountResult);

            boolean storageResult = storageService.reduceByGoodsCode(goodsCode, goodsCount);
            kvResult.put("storageResult", storageResult);

            OrderEntity orderEntity = new OrderEntity().setUserId(userId).setGoodsCode(goodsCode).setCount(goodsCount).setMoney((goodsPrice.longValue()*goodsCount));
            boolean orderResult = orderService.createOrder(orderEntity);
            kvResult.put("orderResult", orderResult);

//            CartEntity cartEntity = new CartEntity().setId(cartId).setCartKey("fky").setSaleType(9).setUserId(Long.parseLong(userId))
//                    .setYn(1).setCreateTime(new Date()).setUpdateTime(new Date());
            // 只要version有值就会触发entity中@TableField注解的update属性"%s+1"
            CartEntity cartEntity = new CartEntity().setId(cartId).setCartKey(RandomUtil.randomString(10)).setVersion(33);
            //cartService.save(cartEntity);
            cartService.updateById(cartEntity);

            if (goodsCount == 1) {
                throw new RuntimeException("测试手动抛出异常后，分布式事务should回滚！");
            }

//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            log.info("{} transaction finish !!!", randomStr);
//        }

        return kvResult;
    }

}
