package com.wumugulu.dubbo.provider.main.service.impl;

import com.wumugulu.dubbo.provider.main.service.MainService;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service("DemoServiceBean")
public class DemoServiceImpl implements MainService {

    @Override
    public String sayHello(String name) {
        return "hello " + name + ", ddkyVersion = " + "ddkyVersion";
    }

    @Override
    public Map<String, Object> testComplexData(Map<String, Object> paraMap) {
        return null;
    }

    @Override
    public boolean initMallData(Map<String, Object> paraMap) {
        return false;
    }

    @Override
    public Map<String, Object> testRedissonLock(Map<String, Object> paraMap) {
        return null;
    }

    @Override
    public Map<String, Object> testTransaction(Map<String, Object> paraMap) {
        return null;
    }
}
