package com.wumugulu.dubbo.provider.main;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.wumugulu.dubbo.basedata.entity.account.AccountEntity;
import com.wumugulu.dubbo.provider.account.service.AccountService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class ProviderImplTest {

    @DubboReference
    private AccountService accountService;

    @Test
    void save() {
        // INSERT INTO b_ccount ( id, name, lev ) VALUES ( ?, ?, ? )
        AccountEntity accountEntity = new AccountEntity()
                .setId(888888)
                .setUserId(55)
                .setMoney(888L);

        boolean result = accountService.save(accountEntity);
        System.out.println("[save] result = {" + result + "}");
    }

    @Test
    void update() {
        // UPDATE b_address SET name=?, lev=? WHERE id=?
        AccountEntity addressEntity = new AccountEntity()
                .setId(888888)
                .setUserId(55)
                .setMoney(888L);

        boolean result = accountService.updateById(addressEntity);
        System.out.println("[updateById] result = {" + result + "}");
    }

    @Test
    void updateWrapper() {
        // UPDATE b_address SET post_code=?,lev=? WHERE (id = ?)
        LambdaUpdateWrapper<AccountEntity> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(AccountEntity::getId, "999999")
                    .set(AccountEntity::getUserId, "111222")
                    .set(AccountEntity::getMoney, 888L);

        boolean result = accountService.update(updateWrapper);
        System.out.println("[update] result = {" + result + "}");
    }

    @Test
    void deleteWrapper() {
        // DELETE FROM b_address WHERE (id = ? AND lev = ?)
        LambdaUpdateWrapper<AccountEntity> deleteWrapper = new LambdaUpdateWrapper<>();
        deleteWrapper.eq(AccountEntity::getId, "999999")
                    .eq(AccountEntity::getUserId, 888);

        boolean result = accountService.remove(deleteWrapper);
        System.out.println("[remove] result = {" + result + "}");
    }

    @Test
    void queryWrapper() {
        //
        LambdaQueryWrapper<AccountEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AccountEntity::getId, "999999")
                    .eq(AccountEntity::getUserId, 888);

        List<AccountEntity> entityList = accountService.list(queryWrapper);
        System.out.println("[remove] result = {" + entityList + "}");
    }

}
