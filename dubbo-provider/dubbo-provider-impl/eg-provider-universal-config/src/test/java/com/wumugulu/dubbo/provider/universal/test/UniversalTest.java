package com.wumugulu.dubbo.provider.universal.test;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;

//@SpringBootTest
@Slf4j
public class UniversalTest {

    @Test
    public void testHashMap() {
        HashMap<String , Object> paraMap = new HashMap<>();
        paraMap.put("key", "zzz");
        paraMap.put("sign", 992);
        System.err.println("json = " + JSON.toJSONString(paraMap));
        log.info("Service: --IN-- {}", JSON.toJSONString(paraMap));
    }


    @Test
    public void testJSON() {
        Integer obj = null;
        System.err.println(JSON.toJSONString(obj));
    }
}
