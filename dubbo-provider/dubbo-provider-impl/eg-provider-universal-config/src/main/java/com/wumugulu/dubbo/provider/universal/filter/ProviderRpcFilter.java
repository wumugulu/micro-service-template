package com.wumugulu.dubbo.provider.universal.filter;

import com.wumugulu.dubbo.common.regular.Constant;
import com.wumugulu.dubbo.common.context.UserContext;
import com.wumugulu.dubbo.common.context.UserDetail;
import org.apache.dubbo.common.constants.CommonConstants;
import org.apache.dubbo.common.extension.Activate;
import org.apache.dubbo.rpc.Filter;
import org.apache.dubbo.rpc.Invocation;
import org.apache.dubbo.rpc.Invoker;
import org.apache.dubbo.rpc.Result;
import org.apache.dubbo.rpc.RpcContext;
import org.apache.dubbo.rpc.RpcException;

import java.util.Date;

@Activate(group = CommonConstants.PROVIDER)
public class ProviderRpcFilter implements Filter {

    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        // receive UserDetail Object from dubbo Consumer via RpcContext
        UserDetail userDetail = (UserDetail) RpcContext.getContext().getObjectAttachment(Constant.DUBBO_KEY_USER_DETAIL);
        System.err.println("ProviderDubboFilter: read from dubbo's RpcContext, and Put into provider's UserContext = " + userDetail);
        if (userDetail == null) {
            userDetail = new UserDetail();
            userDetail.setId(0L);
            userDetail.setUsername("wumugulu");
            userDetail.setLoginTime(new Date());
            userDetail.setCompany("www.seven.com");
        }
        UserContext.setObject(userDetail);

        return invoker.invoke(invocation);
    }

}
