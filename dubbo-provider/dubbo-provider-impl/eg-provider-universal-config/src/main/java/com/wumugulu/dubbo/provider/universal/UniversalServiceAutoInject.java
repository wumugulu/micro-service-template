package com.wumugulu.dubbo.provider.universal;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = {"com.wumugulu.dubbo.provider.universal"})
public class UniversalServiceAutoInject {
}
