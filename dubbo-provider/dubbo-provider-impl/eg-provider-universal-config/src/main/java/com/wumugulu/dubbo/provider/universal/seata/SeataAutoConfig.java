package com.wumugulu.dubbo.provider.universal.seata;

import io.seata.spring.annotation.GlobalTransactionScanner;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * 使用seata-all时需要手动配置GlobalTransactionScanner
 * 使用seata-spring-boot-starter则无需额外配置
 *
 */
@Configuration
public class SeataAutoConfig {

    @Value("${practice.username:NullValue}")
    private String configUsername;
    @Value("${practice.domain:NullValue}")
    private String configDomain;

    @Bean(name = "globalConfig")
    public Map<String, Object> getGlobalConfig() {
        System.err.println("provider/universal/SeataAutoConfig getGlobalConfig init ...... " + configUsername + ": " + configDomain);
        HashMap<String, Object> map = new HashMap<>();
        map.put("username", configUsername);
        map.put("domain", configDomain);
        return map;
    }

    /**
     * 这里之前是注释掉@Bean的.时间太久,忘记为啥了
     * @return
     */
    @Bean
    public GlobalTransactionScanner globalTransactionScanner(){
        System.err.println("provider/universal/SeataAutoConfig globalTransactionScanner bean ......");
        return new GlobalTransactionScanner("cart-gts-seata-example", "eg_mall_tx_group");
    }

}
