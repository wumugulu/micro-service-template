package com.wumugulu.dubbo.provider.universal.aspect;

import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import java.util.HashMap;
import java.util.Map;

/**
 * 由于代理的问题,无法用AOP实现对@Service或@DubboService注解的切片,
 * 除非使用"execution (* com.wumugulu.zkrouter.service.readProduct.impl..*.*(..))"这样的切片
 * 得用Filter实现日志记录的功能!!!
 */
//@Aspect
//@Component
@Slf4j
public class ProviderLogAspect {

//    @Pointcut("@annotation(org.apache.dubbo.config.annotation.DubboService)")
    @Pointcut("@annotation(org.springframework.stereotype.Service)")
//    @Pointcut("execution (* com.wumugulu.zkrouter.service.readProduct.impl..*.*(..))")
    public void dubboServiceMethod(){}

    @Around("dubboServiceMethod()")
    public Object doAround(ProceedingJoinPoint pjp) throws Throwable {
        logMethodInput(pjp);
        StopWatch sw = new StopWatch("dubboServiceMethod");
        sw.start();
        Object result = null;
        try {
            result = pjp.proceed();
            logMethodOutput(sw, result);
        } catch (Exception e) {
            logMethodOutput(sw, e);
            throw e;
        }
        return result;
    }

    private void logMethodInput(ProceedingJoinPoint point) {
        String[] methodParaNames = ((MethodSignature) point.getSignature()).getParameterNames();
        Object[] methodParaValues = point.getArgs();

        Map<String , Object> paraMap = new HashMap<>();
        for (int i = 0; i < methodParaNames.length; i++) {
            paraMap.put(methodParaNames[i], methodParaValues[i]);
        }

        log.info("Service: [[IN ]] {}, {}, {}", point.getSignature().getDeclaringTypeName(), point.getSignature().getName(), JSONUtil.toJsonStr(paraMap));
    }

    private void logMethodOutput(StopWatch sw, Object ret) {
        sw.stop();
        String logContent;
        if (ret instanceof Exception)
            logContent = ((Exception) ret).getMessage();
        else
            logContent = JSONUtil.toJsonStr(ret);

        log.info("Service: [[OUT]] {}ms, {}, {}", sw.getLastTaskTimeMillis(), (ret==null)?"":ret.getClass().getName(), logContent);
    }
    private void logMethodOutput1(long sTime, Object ret) {
        String logContent;
        if (ret instanceof Exception)
            logContent = ((Exception) ret).getMessage();
        else
            logContent = JSONUtil.toJsonStr(ret);

        log.info("Service: [[OUT]] {}ms, {}, {}", (System.currentTimeMillis()-sTime), (ret==null)?"":ret.getClass().getName(), logContent);
    }

}
