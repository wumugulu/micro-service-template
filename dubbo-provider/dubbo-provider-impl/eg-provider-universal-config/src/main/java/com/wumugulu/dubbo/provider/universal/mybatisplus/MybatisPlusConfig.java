package com.wumugulu.dubbo.provider.universal.mybatisplus;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.handler.TableNameHandler;
import com.baomidou.mybatisplus.extension.plugins.inner.DynamicTableNameInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.TenantLineInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.pagination.DialectFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;

/**
 * <p>
 * mybatis-plus 配置
 * </p>
 *
 * @author 黑山老妖
 * @since 某年某月的某一天
 */
@Configuration
public class MybatisPlusConfig {

    public static ThreadLocal<String> TABLE_NAME_SUFFIX = new ThreadLocal<>();

    /**
     *
     * 注意:
     * 使用多个功能需要注意顺序关系,建议使用如下顺序
     *
     * 多租户,动态表名
     * 分页,乐观锁
     * sql性能规范,防止全表更新与删除
     *
    */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        System.err.println("provider/universal/mybatis/mybatisPlusInterceptor init ......");

        // 租户插件
        // 根据 handler 的处理规则,对设定的表(字段)进行租户条件过滤
        TenantLineInnerInterceptor tenantLineInnerInterceptor = new TenantLineInnerInterceptor();
        tenantLineInnerInterceptor.setTenantLineHandler(new MybatisPlusTenant());

        // 动态表名插件
        // 这里定义了两个表 "b_cart" 和 "b_table_need_dynamic_name",碰到这两个表时,会根据方法体更换为新的表名
        DynamicTableNameInnerInterceptor dynamicTableNameInnerInterceptor = new DynamicTableNameInnerInterceptor();
        dynamicTableNameInnerInterceptor.setTableNameHandlerMap(getTableNameHandlerMap());

        // 分页插件
        PaginationInnerInterceptor paginationInnerInterceptor = new PaginationInnerInterceptor(DbType.MYSQL);
        paginationInnerInterceptor.setOverflow(true);// 设置请求的页面大于最大页后操作， true调回到首页，false 继续请求  默认false
        paginationInnerInterceptor.setMaxLimit(-1L);// 设置最大单页限制数量，默认 500 条，-1 不受限制
        paginationInnerInterceptor.setDialect(DialectFactory.getDialect(DbType.MYSQL));// 开启 count 的 join 优化,只针对部分 left join

        // 设置mybatisplus拦截器,注意顺序!!!
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(tenantLineInnerInterceptor);
        interceptor.addInnerInterceptor(dynamicTableNameInnerInterceptor);
        interceptor.addInnerInterceptor(paginationInnerInterceptor);

        return interceptor;
    }

    private HashMap<String, TableNameHandler> getTableNameHandlerMap() {
        HashMap<String, TableNameHandler> tableNameHandlerHashMap = new HashMap<>();
        tableNameHandlerHashMap.put("b_cart", (sql, tableName) -> {
            if (StringUtils.isBlank( TABLE_NAME_SUFFIX.get())) {
                return "b_cart" + "_unspecified";
            } else {
                return "b_cart" + "_" + TABLE_NAME_SUFFIX.get();
            }
        });
        tableNameHandlerHashMap.put("b_table_need_dynamic_name", (sql, tableName) -> {
            if (StringUtils.isBlank( TABLE_NAME_SUFFIX.get())) {
                return "b_table_need_dynamic_name" + "_unspecified";
            } else {
                return "b_table_need_dynamic_name" + "_" + TABLE_NAME_SUFFIX.get();
            }
        });
        return tableNameHandlerHashMap;
    }

}
