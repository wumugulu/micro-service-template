package com.wumugulu.dubbo.provider.universal.mybatisplus;

import com.baomidou.mybatisplus.extension.plugins.handler.TenantLineHandler;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.StringValue;

public class MybatisPlusTenant implements TenantLineHandler {

    private String tableName;

    @Override
    public Expression getTenantId() {
        System.err.println("MybatisPlusTenantLineHandler.getTenantId");
        return new StringValue("1");
        // return null;
    }

    @Override
    public String getTenantIdColumn() {
        System.err.println("MybatisPlusTenantLineHandler.getTenantIdColumn -- " + this.tableName);
        return "status";
        // return TenantLineHandler.super.getTenantIdColumn();
    }

    @Override
    public boolean ignoreTable(String tableName) {
        System.err.println("MybatisPlusTenantLineHandler.ignoreTable -- " + tableName);
        this.tableName = tableName;

        return !"sys_user".equals(tableName);
        // return TenantLineHandler.super.ignoreTable(tableName);
    }
}
