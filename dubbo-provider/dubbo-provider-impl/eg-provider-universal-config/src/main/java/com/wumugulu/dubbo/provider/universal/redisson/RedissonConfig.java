package com.wumugulu.dubbo.provider.universal.redisson;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.redisson.config.SingleServerConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RedissonConfig {

    @Value("${spring.redis.host}")
    private String REDIS_HOST;

    @Value("${spring.redis.port}")
    private Integer REDIS_PORT;

    @Value("${spring.redis.password}")
    private String REDIS_PASSWORD;

    @Bean
    public RedissonClient redissonClient() {
        System.err.println("provider/universal/RedissonConfig redissonClient init ...... (" + REDIS_HOST + ":" + REDIS_PORT + ")");
        String address = "redis://" + REDIS_HOST + ":" + REDIS_PORT;
        //192.168.89.27:6379 ddyy#888
        Config config = new Config();
        SingleServerConfig singleServerConfig = config.useSingleServer();
        singleServerConfig.setAddress(address);// ("redis://192.168.89.27:6379");
        singleServerConfig.setPassword(REDIS_PASSWORD);// ("ddyy#888");
        return Redisson.create(config);
    }

}
