package com.wumugulu.dubbo.provider.universal.filter;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Console;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.TypeUtil;
import cn.hutool.json.JSONUtil;
import com.yomahub.tlog.dubbo.filter.TLogDubboFilter;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.common.constants.CommonConstants;
import org.apache.dubbo.common.extension.Activate;
import org.apache.dubbo.rpc.Filter;
import org.apache.dubbo.rpc.Invocation;
import org.apache.dubbo.rpc.Invoker;
import org.apache.dubbo.rpc.Result;
import org.apache.dubbo.rpc.RpcContext;
import org.apache.dubbo.rpc.RpcException;
import org.apache.dubbo.rpc.support.AccessLogData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 用于记录dubboService调用的日志,包括输入和输出参数,以便快速定位问题;
 */
@Activate(group = CommonConstants.PROVIDER)
public class ProviderLogFilter implements Filter {

    private static final Logger log = LoggerFactory.getLogger(ProviderLogFilter.class);

    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        StopWatch sw = new StopWatch();
        sw.start();
        Result result;
        try {
            RpcLogData rpcLogData = new RpcLogData(invoker, invocation);
            log.info("Service: [[IN ]] {}, ({}:{}){}, {}, {}",
                    rpcLogData.remoteHost, rpcLogData.group, rpcLogData.version,
                    rpcLogData.serviceName, rpcLogData.methodName,
                    JSONUtil.toJsonStr(rpcLogData.getArgsList()));
            result = invoker.invoke(invocation);
        } finally {
            sw.stop();
        }
        log.info("Service: [[OUT]] {}ms, {}, {}",
                sw.getTotalTimeMillis(),
                (result==null || result.getValue()==null) ? "null" : result.getValue().getClass().getName(),
                (result==null || result.getValue()==null) ? "null" : JSONUtil.toJsonStr( result.getValue()));


        return result;
    }

    class RpcLogData {
        private String localHost;
        private Integer localPort;
        private String remoteHost;
        private Integer remotePort;
        private Date invokeTime;
        private String serviceName;
        private String methodName;
        private String group;
        private String version;
        private Class[] types;
        private Object[] arguments;


        private RpcLogData(Invoker<?> invoker, Invocation invocation) {
            RpcContext context = RpcContext.getContext();
            this.localHost = context.getLocalHost();
            this.localPort = context.getLocalPort();
            this.remoteHost = context.getRemoteHost();
            this.remotePort = context.getRemotePort();
            this.invokeTime = new Date();

            this.serviceName = invoker.getInterface().getName();
            this.methodName = invocation.getMethodName();
            this.group = invoker.getUrl().getParameter("group");
            this.version = invoker.getUrl().getParameter("version");
            this.types = invocation.getParameterTypes();
            this.arguments = invocation.getArguments();
        }

        public List<String> getArgsList() {
            List<String> argumentStrList = new ArrayList<>();
            if (ArrayUtil.isNotEmpty(this.types)) {
                for (int i = 0; i < this.types.length; i++) {
                    String tmp = ObjectUtil.isBasicType( this.arguments[i]) ? this.arguments[i].toString() : JSONUtil.toJsonStr(this.arguments[i]);
                    tmp += "(" + this.types[i].getName() + ")";
                    argumentStrList.add(tmp);
                }
            }
            return argumentStrList;
        }

    }

}
