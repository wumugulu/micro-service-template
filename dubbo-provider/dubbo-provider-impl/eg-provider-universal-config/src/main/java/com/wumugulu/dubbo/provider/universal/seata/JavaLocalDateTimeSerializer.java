package com.wumugulu.dubbo.provider.universal.seata;

import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.seata.rm.datasource.undo.parser.spi.JacksonSerializer;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 *
 * mysql's datetime type will mapping to java8's LocalDateTime
 * when Jackson record undo_log into database, Serializer and Deserializer will trigger Exception !!!
 *
 *  1. 引入kryo解决 https://baijiahao.baidu.com/s?id=1700872368390839990&wfr=spider&for=pc
 *  2. 重写JacksonUndoLogParser类解决 https://blog.csdn.net/sssdal19995/article/details/114238928
 *  现在用的就是下面第3方法
 *  3. 增量引入针对LocalDateTime的序列化方法解决: https://blog.csdn.net/zh0134/article/details/115612416
 *
 */
public class JavaLocalDateTimeSerializer implements JacksonSerializer<LocalDateTime> {

    private static final DateTimeFormatter DATETIME_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @Override
    public Class<LocalDateTime> type() {
        return LocalDateTime.class;
    }

    @Override
    public JsonSerializer<LocalDateTime> ser() {
        return new LocalDateTimeSerializer(DATETIME_FORMAT);
    }

    @Override
    public JsonDeserializer<? extends LocalDateTime> deser() {
        return new LocalDateTimeDeserializer(DATETIME_FORMAT);
    }
}
