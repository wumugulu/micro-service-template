package com.wumugulu.dubbo.provider.universal.mybatisplus;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.Map;

/**
 * <p>
 * mybatis-plus 直接从map中获取到分页所需参数
 * </p>
 *
 * @author 黑山老妖
 * @since 某年某月的某一天
 */
public class MybatisPlusPageUtil {

    public static final String PAGE_NUM = "_page";
    public static final String PAGE_SIZE = "_limit";

    public static Page getPage(Map<String, Object> params) {
        long pageNum = 1L;
        long pageSize = 20L;

        if (params.get(PAGE_NUM) != null) { pageNum = Long.parseLong(String.valueOf( params.get(PAGE_NUM))); }
        if (params.get(PAGE_SIZE) != null) { pageSize = Long.parseLong(String.valueOf( params.get(PAGE_SIZE))); }

        return new Page(pageNum, pageSize);
    }
}
