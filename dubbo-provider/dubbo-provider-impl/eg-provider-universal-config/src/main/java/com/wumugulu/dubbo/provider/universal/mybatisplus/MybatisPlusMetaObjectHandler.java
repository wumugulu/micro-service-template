package com.wumugulu.dubbo.provider.universal.mybatisplus;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class MybatisPlusMetaObjectHandler implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        System.err.println("自动在insert时填充默认值 -- " + metaObject.getOriginalObject().getClass().getName() + ":  " + metaObject.getOriginalObject());
        // this.setFieldValByName("updateTime", new Date(), metaObject);
        this.fillStrategy(metaObject, "createTime", new Date())
                .fillStrategy(metaObject, "updateTime", new Date())
                .fillStrategy(metaObject, "selfTest", 33);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        System.err.println("自动在update时填充默认值 -- " + metaObject.getOriginalObject().getClass().getName() + ":  " + metaObject.getOriginalObject());
        //this.setFieldValByName("updateTime", new Date(), metaObject);
        this.fillStrategy(metaObject, "updateTime", new Date())
                .fillStrategy(metaObject, "selfTest", 44);

        if (metaObject.getOriginalObject().getClass().getName().equals("com.wumugulu.dubbo.provider.cart.entity.CartEntity")) {
            System.err.println("metaObject.getValue(\"version\") = " + metaObject.getValue("version"));
            // entity 中的 version 属性得加上 @TableField(value = "version", fill = FieldFill.INSERT), 否则不会生效
            this.fillStrategy(metaObject, "version", 31);
        }
    }
}
