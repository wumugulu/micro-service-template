package com.wumugulu.dubbo.provider.cart.service.impl;

import cn.hutool.core.map.MapUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wumugulu.dubbo.basedata.entity.order.OrderEntity;
import com.wumugulu.dubbo.provider.universal.mybatisplus.MybatisPlusConfig;
import com.wumugulu.dubbo.provider.universal.mybatisplus.MybatisPlusPageUtil;
import com.wumugulu.dubbo.basedata.entity.cart.CartEntity;
import com.wumugulu.dubbo.provider.cart.mapper.CartMapper;
import com.wumugulu.dubbo.provider.cart.service.CartService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 购物车 服务实现类
 * </p>
 *
 * @author 黑山老妖
 * @since 2021-05-07
 */
@Service
@DubboService
public class CartServiceImpl extends ServiceImpl<CartMapper, CartEntity> implements CartService {

    @Override
    public Integer countCartBySaleType(Integer saleType) {
        return baseMapper.countCartBySaleType(saleType);
    }

    @Override
    public Integer countAnyTable(String tableName) {
        return baseMapper.countAnyTable(tableName);
    }

    @Override
    public List<CartEntity> queryList(Map<String, Object> paraMap) {
        Wrapper<CartEntity> wrapper = getWrapper(paraMap);

        List<CartEntity> entityList = list(wrapper);// baseMapper.selectList(wrapper);
        return entityList;
    }

    @Override
    public IPage<CartEntity> queryPage(Map<String, Object> paraMap) {
        Page<CartEntity> page = MybatisPlusPageUtil.getPage(paraMap);
        Wrapper<CartEntity> wrapper = getWrapper(paraMap);

        IPage<CartEntity> entityPage = page(page, wrapper);// baseMapper.selectPage(page, wrapper);
        return entityPage;
    }

    private Wrapper<CartEntity> getWrapper(Map<String, Object> paramMap) {
        Map<String, Object> kvMap = new HashMap<>();
        kvMap.putAll(paramMap);

        LambdaQueryWrapper<CartEntity> wrapper = new LambdaQueryWrapper<>();

        Long userId = MapUtil.getLong(kvMap, "userId");
        wrapper.eq(userId != null, CartEntity::getUserId, userId);

        Integer saleType = MapUtil.getInt(kvMap, "saleType");
        if (saleType != null) { wrapper.eq(CartEntity::getSaleType, saleType); }

        //设置动态表名的后缀!!!
        String tableId = MapUtil.getStr(kvMap, "tableId");
        if (tableId != null) { MybatisPlusConfig.TABLE_NAME_SUFFIX.set(tableId); }

        wrapper.orderByDesc(CartEntity::getSaleType).orderByAsc(CartEntity::getCreateTime);
        return wrapper;
    }

}
