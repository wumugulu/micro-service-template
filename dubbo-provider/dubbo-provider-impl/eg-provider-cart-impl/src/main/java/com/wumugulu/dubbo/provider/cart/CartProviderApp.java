package com.wumugulu.dubbo.provider.cart;

import com.yomahub.tlog.core.enhance.bytes.AspectLogEnhance;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableDubbo
public class CartProviderApp {

    static { AspectLogEnhance.enhance(); }

    public static void main(String[] args) {
        SpringApplication.run(CartProviderApp.class, args);
    }

}
