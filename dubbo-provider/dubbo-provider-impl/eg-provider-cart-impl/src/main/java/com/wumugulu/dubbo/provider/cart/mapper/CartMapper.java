package com.wumugulu.dubbo.provider.cart.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wumugulu.dubbo.basedata.entity.cart.CartEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * 购物车 Mapper 接口
 * </p>
 *
 * @author 黑山老妖
 * @since 2021-06-22
 */
@Mapper
public interface CartMapper extends BaseMapper<CartEntity> {

    @Select("select count(1) from t_cart where sale_type = #{saleType}")
    Integer countCartBySaleType(@Param("saleType") Integer saleType);

    @Select("select count(1) from ${tableName} ")
    Integer countAnyTable(@Param("tableName") String tableName);

}
