package com.wumugulu.dubbo.provider.order.service.impl;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wumugulu.dubbo.provider.universal.mybatisplus.MybatisPlusConfig;
import com.wumugulu.dubbo.provider.universal.mybatisplus.MybatisPlusPageUtil;
import com.wumugulu.dubbo.basedata.entity.order.OrderEntity;
import com.wumugulu.dubbo.provider.order.mapper.OrderMapper;
import com.wumugulu.dubbo.provider.order.service.OrderService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 黑山老妖
 * @since 2021-06-22
 */
@Service
@DubboService
public class OrderServiceImpl extends ServiceImpl<OrderMapper, OrderEntity> implements OrderService {

    @Override
    public boolean createOrder(OrderEntity orderEntity) {
//        if (orderEntity.getCount() == 1) {
//            orderEntity.setMoney(Long.parseLong("a1000"));
//            // throw new RuntimeException("orderEntity.getCount() == 1, trigger a Exception");
//        }

        return save(orderEntity);
    }

    @Override
    public List<OrderEntity> queryList(Map<String, Object> paraMap) {
        Wrapper<OrderEntity> wrapper = getWrapper(paraMap);

        List<OrderEntity> entityList = list(wrapper);// baseMapper.selectList(wrapper);
        return entityList;
    }

    @Override
    public IPage<OrderEntity> queryPage(Map<String, Object> paraMap) {
        Page<OrderEntity> page = MybatisPlusPageUtil.getPage(paraMap);
        Wrapper<OrderEntity> wrapper = getWrapper(paraMap);

        IPage<OrderEntity> entityPage = page(page, wrapper);// baseMapper.selectPage(page, wrapper);
        return entityPage;
    }

    private Wrapper<OrderEntity> getWrapper(Map<String, Object> paramMap) {
        Map<String, Object> kvMap = new HashMap<>();
        kvMap.putAll(paramMap);

        LambdaQueryWrapper<OrderEntity> wrapper = new LambdaQueryWrapper<>();

        Long userId = MapUtil.getLong(kvMap, "userId");
        wrapper.eq(userId != null, OrderEntity::getUserId, userId);

        String goodsCode = MapUtil.getStr(kvMap, "goodsCode");
        wrapper.eq(StrUtil.isNotBlank(goodsCode), OrderEntity::getGoodsCode, goodsCode);

        //设置动态表名的后缀!!!
        String tableId = MapUtil.getStr(kvMap, "tableId");
        if (tableId != null) { MybatisPlusConfig.TABLE_NAME_SUFFIX.set(tableId); }

        wrapper.orderByDesc(OrderEntity::getId).orderByAsc(OrderEntity::getMoney);
        return wrapper;
    }

}
