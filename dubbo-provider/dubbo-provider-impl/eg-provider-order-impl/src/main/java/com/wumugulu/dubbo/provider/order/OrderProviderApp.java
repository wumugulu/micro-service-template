package com.wumugulu.dubbo.provider.order;

import com.yomahub.tlog.core.enhance.bytes.AspectLogEnhance;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableDubbo
public class OrderProviderApp {

    static { AspectLogEnhance.enhance(); }

    public static void main(String[] args) {
        SpringApplication.run(OrderProviderApp.class, args);
    }

}
