package com.wumugulu.dubbo.provider.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wumugulu.dubbo.basedata.entity.order.OrderEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 黑山老妖
 * @since 2021-06-22
 */
@Mapper
public interface OrderMapper extends BaseMapper<OrderEntity> {

}
