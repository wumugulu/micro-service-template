package com.wumugulu.dubbo.provider.storage.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wumugulu.dubbo.basedata.entity.storage.StorageEntity;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 黑山老妖
 * @since 2021-06-22
 */
public interface StorageService extends IService<StorageEntity> {

    public boolean reduceByGoodsCode(String goodsCode, Integer count);

    public List<StorageEntity> queryList(Map<String, Object> paraMap);

    public IPage<StorageEntity> queryPage(Map<String, Object> paraMap);
}
