package com.wumugulu.dubbo.provider.account.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wumugulu.dubbo.basedata.entity.account.AccountEntity;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 黑山老妖
 * @since 2021-06-22
 */
public interface AccountService extends IService<AccountEntity> {

    public boolean debtByUserId(Integer userId, Integer money);

    public List<AccountEntity> queryList(Map<String, Object> paraMap);

    public IPage<AccountEntity> queryPage(Map<String, Object> paraMap);

    public Integer offsetMoneyById(Integer id, Long amount);

}
