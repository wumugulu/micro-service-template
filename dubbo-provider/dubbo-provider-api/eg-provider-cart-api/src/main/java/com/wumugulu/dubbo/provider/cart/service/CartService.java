package com.wumugulu.dubbo.provider.cart.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wumugulu.dubbo.basedata.entity.cart.CartEntity;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 购物车 服务类
 * </p>
 *
 * @author 黑山老妖
 * @since 2021-06-22
 */
public interface CartService extends IService<CartEntity> {

    public Integer countCartBySaleType(Integer saleType);
    public Integer countAnyTable(String tableName);

    public List<CartEntity> queryList(Map<String, Object> paraMap);

    public IPage<CartEntity> queryPage(Map<String, Object> paraMap);

}
