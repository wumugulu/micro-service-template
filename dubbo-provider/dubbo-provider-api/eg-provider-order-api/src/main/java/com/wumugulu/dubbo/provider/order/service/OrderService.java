package com.wumugulu.dubbo.provider.order.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wumugulu.dubbo.basedata.entity.order.OrderEntity;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 黑山老妖
 * @since 2021-06-22
 */
public interface OrderService extends IService<OrderEntity> {

    public boolean createOrder(OrderEntity orderEntity);

    public List<OrderEntity> queryList(Map<String, Object> paraMap);

    public IPage<OrderEntity> queryPage(Map<String, Object> paraMap);

}
