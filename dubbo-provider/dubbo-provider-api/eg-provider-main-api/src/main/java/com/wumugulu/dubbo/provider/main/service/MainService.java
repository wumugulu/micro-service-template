package com.wumugulu.dubbo.provider.main.service;

import java.util.Map;

public interface MainService {

    public String sayHello(String name);

    public Map<String, Object> testComplexData(Map<String, Object> paraMap);

    public boolean initMallData(Map<String, Object> paraMap);

    public Map<String, Object> testRedissonLock(Map<String, Object> paraMap);

    public Map<String, Object> testTransaction(Map<String, Object> paraMap);
}
