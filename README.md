# MicroServiceTemplate

#### 介绍
微服务架构-项目模板

#### 软件架构
软件架构说明
>springboot2.4.5 + mysql5.7 + mybatisplus3.4.2 + druid1.2.5 + 
> dubbo2.7 + redis + redisson + seata1.4.2


#### 安装教程

1.  mysql5.7 - *
2.  Redis(暂未启用)
3.  ZooKeeper3.5.9 - *
4.  ZooInspector(暂未启用)
5.  Seata-Server1.4.2 - * (8091端口)
    如果采用file模式,seata会在自己的bin目录下生成sessionStore/root.data用于存储相关信息以备事务的回滚

#### 使用说明

1.  统一管理配置文件
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
