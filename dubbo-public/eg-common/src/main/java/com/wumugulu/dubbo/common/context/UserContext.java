package com.wumugulu.dubbo.common.context;

public class UserContext {

    private static ThreadLocal<UserDetail> TL = ThreadLocal.withInitial(()->new UserDetail());

    public static void setObject(UserDetail userDetail) {
        UserContext.TL.set(userDetail);
    }

    public static UserDetail getObject() {
        return UserContext.TL.get();
    }

}
