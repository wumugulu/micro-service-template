package com.wumugulu.dubbo.common.context;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Data
@ToString
public class UserDetail implements Serializable {

    private Long id;
    private String username;
    private String company;
    private Date loginTime;

}
