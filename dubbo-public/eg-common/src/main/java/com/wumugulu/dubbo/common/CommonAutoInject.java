package com.wumugulu.dubbo.common;

import org.springframework.context.annotation.ComponentScan;


@ComponentScan(basePackages = {"com.wumugulu.dubbo.common"})
public class CommonAutoInject {

}
