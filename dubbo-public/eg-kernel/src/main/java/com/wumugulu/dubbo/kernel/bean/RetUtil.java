package com.wumugulu.dubbo.kernel.bean;

import cn.hutool.core.lang.Dict;

import java.io.Serializable;

public class RetUtil extends Dict implements Serializable {

    private final static String KEY_CODE = "code";
    private final static String KEY_MSG = "msg";
    private final static String KEY_DATA = "data";

    public static Dict error(Integer code, String msg) {
        return new Dict().set(KEY_CODE, code).set(KEY_MSG, msg);
    }

    public static Dict success() {
        return new Dict().set(KEY_CODE, 0).set(KEY_MSG, "success");
    }

    public static Dict success(Object obj) {
        return success().set(KEY_DATA, obj);
    }

//    public Dict put(String key, Object value) {
//        if (KEY_CODE.equals(key) || KEY_MSG.equals(key) || KEY_CODE.equals(key)) {
//            throw new RuntimeException("invalid key name: " + key);
//        }
//        set(key, value);
//        return this;
//    }

    @Override
    public Dict set(String key, Object value) {
        if (KEY_CODE.equals(key) || KEY_MSG.equals(key) || KEY_CODE.equals(key)) {
            throw new RuntimeException("invalid key name: " + key);
        }
        return super.set(key, value);
    }

    @Override
    public Object put(String key, Object value) {
        if (KEY_CODE.equals(key) || KEY_MSG.equals(key) || KEY_CODE.equals(key)) {
            throw new RuntimeException("invalid key name: " + key);
        }
        return super.put(key, value);
    }
}
