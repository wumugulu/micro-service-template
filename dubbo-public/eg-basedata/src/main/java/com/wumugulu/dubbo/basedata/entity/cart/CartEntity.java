package com.wumugulu.dubbo.basedata.entity.cart;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 购物车
 * </p>
 *
 * @author 黑山老妖
 * @since 2021-06-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_cart")
public class CartEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 密钥
     */
    @TableField("cart_key")
    private String cartKey;

    /**
     * 用户编号
     */
    @TableField("user_id")
    private Integer userId;

    /**
     * 是否有效 1:有效 0:无效
     */
    @TableField("yn")
    private Integer yn;

    /**
     * 销售模式1:控销 2:流通
     */
    @TableField("sale_type")
    private Integer saleType;

    /**
     * 修改版本号,+1递增
     */
    @TableField(value = "version", update = "%s+1")
    private Integer version;

    /**
     * 删除标志
     */
    @TableLogic(value = "0", delval = "1")
    @TableField("is_deleted")
    private Integer isDeleted;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

}
