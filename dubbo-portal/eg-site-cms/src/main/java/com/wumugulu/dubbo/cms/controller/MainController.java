package com.wumugulu.dubbo.cms.controller;

import cn.hutool.core.lang.Dict;
import com.wumugulu.dubbo.common.kit.StrKit;
import com.wumugulu.dubbo.kernel.bean.RetUtil;
import com.wumugulu.dubbo.provider.main.service.MainService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @description:
 * @Author: FengKaiYu
 * @Create: 2021-04-28
 */
@RestController
@RequestMapping("/main")
//@RefreshScope
@Slf4j
public class MainController {

    @Value("${order.diyname:not set}")
    private String diyName;//="can not get from dubbo-admin's config-center";

    @Autowired
    @Qualifier("PortalConfigBean")
    Map<String, Object> portalConfigBean;

    @DubboReference
    private MainService mainService;

//    @Autowired
//    private ConfigCenter configCenter;

    @RequestMapping("/portalConfig")
    public Object portalConfig() {
        return portalConfigBean;
    }

    @RequestMapping("/config")
    public String config() {
        String configCenterDiyname = diyName;//configCenter.getDiyname();
        return mainService.sayHello(configCenterDiyname);
    }

    @RequestMapping("/local")
     public String local(@RequestParam(name = "name", defaultValue = "NotSet") String name) {
         return mainService.sayHello(name);
     }

    @RequestMapping("/remote")
    public String remote(@RequestParam(name = "name", defaultValue = "NotSet") String name) {
        log.info("consumer.main.remote: name={}", name);
        if (StrKit.isBlank(name)){
            name = "anonymous";
        }
        return mainService.sayHello(name);
    }

    @RequestMapping("/initMallData")
    public Dict initMallData(@RequestBody Map<String, Object> paraMap) {
        log.info("consumer.mainController.initMallData: paraMap={}", paraMap);

        return RetUtil.success( mainService.initMallData(paraMap));
    }

    @RequestMapping("/testComplexData")
    public Dict testComplexData(@RequestBody Map<String, Object> paraMap) {
        log.info("consumer.mainController.testComplexData: paraMap={}", paraMap);

        return RetUtil.success( mainService.testComplexData(paraMap));
    }

    @RequestMapping("/testRedissonLock")
    public Dict testRedissonLock(@RequestBody Map<String, Object> paraMap) {
        log.info("consumer.mainController.testRedissonLock: paraMap={}", paraMap);

        return RetUtil.success( mainService.testRedissonLock(paraMap));
    }

    @RequestMapping("/testTransaction")
    public Dict testTransaction(@RequestBody Map<String, Object> paraMap) {
        log.info("consumer.mainController.testTransaction: paraMap={}", paraMap);

        return RetUtil.success( mainService.testTransaction(paraMap));
    }

}
