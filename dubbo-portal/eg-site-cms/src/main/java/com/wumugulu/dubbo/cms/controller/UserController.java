package com.wumugulu.dubbo.cms.controller;


import cn.hutool.core.lang.Dict;
import cn.hutool.core.map.MapUtil;
import com.wumugulu.dubbo.kernel.bean.RetUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @description:
 * @Author: FengKaiYu
 * @Create: 2021-04-28
 */
@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {

    @Autowired
    @Qualifier("PortalConfigBean")
    Map<String, Object> portalConfigBean;

    @RequestMapping("/login")
    public Dict login(@RequestBody Map<String, Object> kv) {
        //return Ret.error(300, "not found the user");
        log.info("{}, {}", kv, MapUtil.getStr(kv, "username"));
        return RetUtil.success();
    }
    public Dict login2(@RequestBody String reqBody) {
        //return Ret.error(300, "not found the user");
        log.info("{}, {}", reqBody);
        return RetUtil.success();
    }


    //    @ApiOperation(value="PC关联字典-接口简介", notes="PC关联字典-接口<b><a href='#'>详细</a></b>说明")
//    @ApiImplicitParams({
//        @ApiImplicitParam(paramType = "query", name = "lev", value = "地址级别", dataType="Integer"),
//        @ApiImplicitParam(paramType = "query", name = "postCode", value = "邮政编码", dataType = "String"),
//        @ApiImplicitParam(paramType = "query", name = "map", value = "搜索条件", defaultValue = "{'lev': 0}", dataType = "String")
//    })
//    @ResponseBody
    @PostMapping("/testSwagger")
    public String testSwagger(@RequestBody Map<String, Object> map, @RequestParam("uname") String name, @RequestParam("upwd") String pwd) {
        log.info("testSwagger: {}, {}, {}", name, pwd, map);
        return "testSwagger success !";
    }

    @PostMapping("/testPostRequestBody")
    public String testPostRequestBody(@RequestBody Map<String, Object> map, @RequestParam("uname") String name, @RequestParam("upwd") String pwd) {
        log.info("testPostRequestBody: {}, {}, {}", name, pwd, map);
        return "testPostRequestBody success !";
    }

    @PostMapping("/testPostRequestBody2")
    public String testPostRequestBody2(@RequestBody Map<String, Object> map, @RequestParam("uname") String name, @RequestParam("upwd") String pwd) {
        log.info("testPostRequestBody: {}, {}, {}", name, pwd, map);
        return map.toString();
    }

    @PostMapping("/testPostRequestBody3")
    public Map testPostRequestBody3(@RequestBody Map<String, Object> map, @RequestParam("uname") String name, @RequestParam("upwd") String pwd) {
        log.info("testPostRequestBody: {}, {}, {}", name, pwd, map);
        return map;
    }

    @GetMapping("/testSwagger")
    public String testSwaggerGet(@RequestParam("uname") String name, @RequestParam("upwd") String pwd) {
        log.info("testSwagger: {}, {}", name, pwd);
        return "testSwagger success !";
    }

}
