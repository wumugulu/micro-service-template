package com.wumugulu.dubbo.cms;

import com.yomahub.tlog.core.enhance.bytes.AspectLogEnhance;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class CmsSiteApplication {

    static { AspectLogEnhance.enhance(); }

    public static void main(String[] args) {
        SpringApplication.run(CmsSiteApplication.class, args);
    }

}
