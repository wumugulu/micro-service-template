package com.wumugulu.dubbo.web.test.callback;

import cn.hutool.core.util.RandomUtil;

import java.util.Date;

public class Worker {
    public interface WorkListener{
        void onWorkFinish(Integer orderId, Integer count, String result);
    }

    private WorkListener listener;

    public void setWorkListener(WorkListener listener) {
        this.listener = listener;
    }

    public void doWork(Integer orderId, Integer count) throws InterruptedException {
        for (int i=0; i<count; i++) {
            System.err.println(new Date() + ": making 第 " + (i+1) + " 件设备...");
            Thread.sleep(RandomUtil.randomInt(200, 2000));
        }
        listener.onWorkFinish(orderId, count,"success");
    }
}
