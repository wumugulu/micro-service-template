package com.wumugulu.dubbo.web.test.callback;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.RuntimeUtil;

public class Seller {

    public static void main(String[] args) throws InterruptedException {
        Worker worker = new Worker();
        worker.setWorkListener(new Worker.WorkListener() {
            @Override
            public void onWorkFinish(Integer orderId, Integer count, String result) {
                System.err.println("order: " + orderId + ", count: " + count + ", result: " + result);
                sell(orderId, count, result);
            }
        });
        int orderId = (int) (System.currentTimeMillis()%10000);
        int count = RandomUtil.randomInt(1, 5);
        System.err.println("work plan: " + orderId + ", count: " + count);
        worker.doWork(orderId, count);
    }

    private static void sell(Integer orderId, Integer count, String result) {
        System.err.println("order " + orderId + " is " + result + ", there are " + count + " machines can be sell !!!");
        System.err.println(RuntimeUtil.execForStr("ipconfig /all"));
        System.err.println(IdUtil.createSnowflake(1L, 2L).nextIdStr());
        System.err.println(IdUtil.createSnowflake(1L, 2L).nextIdStr());
    }
}
