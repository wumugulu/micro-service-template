package com.wumugulu.dubbo.web;

import com.yomahub.tlog.core.enhance.bytes.AspectLogEnhance;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class WebSiteApplication {

    static { AspectLogEnhance.enhance(); }

    public static void main(String[] args) {
        SpringApplication.run(WebSiteApplication.class, args);
    }

}
