package com.wumugulu.dubbo.web.controller;


import cn.hutool.core.lang.Dict;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.wumugulu.dubbo.kernel.bean.RetUtil;
import com.wumugulu.dubbo.basedata.entity.cart.CartEntity;
import com.wumugulu.dubbo.provider.cart.service.CartService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 购物车 前端控制器
 * </p>
 *
 * @author 黑山老妖
 * @since 2021-05-07
 */
@RestController
@RequestMapping("/cart")
@Slf4j
public class CartController {

    @DubboReference
    private CartService cartService;

    /**
     *
     * @param id = 83890
     * @return
     */
    @RequestMapping("/getById")
    public Dict getById(Integer id, String tableId) {
        CartEntity cartEntity = cartService.getById(id);
        return RetUtil.success(cartEntity);
    }

//    public Ret getByIdInDynamicTable(Integer id, String tableId) {
//        log.info("Controller: {}, {}, {}, {}", "/cart", "/getById", id, tableId);
//        CartEntity cartEntity = cartService.queryById(id, tableId);
//        return Ret.success(cartEntity);
//    }

    /**
     *
     * @param map = {"userId":73189,"saleType":2,"tableId":"001"}
     * @return
     */
    @RequestMapping("/queryList")
    public Dict queryList(@RequestBody Map<String, Object> map) {
        log.info("Controller: {}, {}, {}", "/cart", "/queryList", JSONUtil.toJsonStr(map));
        List<CartEntity> cartList = cartService.queryList(map);
        return RetUtil.success(cartList);
    }

    /**
     *
     * @param map
     * {"userId":73189,"saleType":2,"tableId":"001","_page":2,"_limit":5}
     *
     * @return
     */
    @RequestMapping("/queryPage")
    public Dict queryPage(@RequestBody Map<String, Object> map) {
        log.info("Controller: {}, {}, {}", "/cart", "/queryPage", JSONUtil.toJsonStr(map));
        IPage<CartEntity> cartPage = cartService.queryPage(map);
        return RetUtil.success(cartPage);
    }

}
