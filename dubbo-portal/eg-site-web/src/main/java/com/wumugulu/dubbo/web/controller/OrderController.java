package com.wumugulu.dubbo.web.controller;


import cn.hutool.core.lang.Dict;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.wumugulu.dubbo.kernel.bean.RetUtil;
import com.wumugulu.dubbo.basedata.entity.order.OrderEntity;
import com.wumugulu.dubbo.provider.order.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * null 前端控制器
 * </p>
 *
 * @author 黑山老妖
 * @since 2021-05-07
 */
@RestController
@RequestMapping("/order")
@Slf4j
public class OrderController {

    @DubboReference
    private OrderService orderService;

    @RequestMapping("/getById")
    public Dict queryById(Integer id) {
        OrderEntity orderEntity = orderService.getById(id);
        return RetUtil.success(orderEntity);
    }

    /**
     *
     * @param map = {"userId":73189,"saleType":2,"tableId":"001"}
     * @return
     */
    @RequestMapping("/queryList")
    public Dict queryList(@RequestBody Map<String, Object> map) {
        log.info("Controller: {}, {}, {}", "/order", "/queryList", JSONUtil.toJsonStr(map));
        List<OrderEntity> orderList = orderService.queryList(map);
        return RetUtil.success(orderList);
    }

    /**
     *
     * @param map
     * {"userId":73189,"saleType":2,"tableId":"001","_page":2,"_limit":5}
     *
     * @return
     */
    @RequestMapping("/queryPage")
    public Dict queryPage(@RequestBody Map<String, Object> map) {
        log.info("Controller: {}, {}, {}", "/order", "/queryPage", JSONUtil.toJsonStr(map));
        IPage<OrderEntity> orderPage = orderService.queryPage(map);
        return RetUtil.success(orderPage);
    }

}
