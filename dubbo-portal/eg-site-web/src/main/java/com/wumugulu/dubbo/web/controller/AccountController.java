package com.wumugulu.dubbo.web.controller;

import cn.hutool.core.lang.Dict;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.wumugulu.dubbo.basedata.entity.account.AccountEntity;
import com.wumugulu.dubbo.kernel.bean.RetUtil;
import com.wumugulu.dubbo.provider.account.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * null 前端控制器
 * </p>
 *
 * @author 黑山老妖
 * @since 2021-05-07
 */
@RestController
@RequestMapping("/account")
@Slf4j
public class AccountController {

    @DubboReference
    private AccountService accountService;

    /**
     *
     * @param id = 83890
     * @return
     */
    @RequestMapping("/getById")
    public Dict getById(Integer id) {
        AccountEntity accountEntity = accountService.getById(id);
        return RetUtil.success(accountEntity);
    }

    /**
     *
     * @param map
     * {"lev":2,"postCode":"441422","tableId":"001"}
     *
     * @return
     */
//    @ApiImplicitParams({
//            @ApiImplicitParam(
//                    name = "lev", value = "地址级别", required = true, dataType="Integer"),
//            @ApiImplicitParam(
//                    name = "postCode", value = "邮政编码", required = true, dataType = "String")
//    })
    @PostMapping("/queryList")
    public Dict queryList(@RequestBody Map<String, Object> map) {
        List<AccountEntity> accountList = accountService.queryList(map);
        return RetUtil.success(accountList);
    }

    /**
     *
     * @param map
     * {"lev":2,"postCode":"441422","tableId":"001","_page":2,"_limit":5}
     *
     * @return
     */
    @RequestMapping("/queryPage")
    public Dict queryPage(@RequestBody Map<String, Object> map) {
        IPage<AccountEntity> accountPage = accountService.queryPage(map);
        return RetUtil.success(accountPage);
    }

    @RequestMapping("/queryPageSearch")
    public Dict queryPageSearch(@RequestBody Map<String, Object> map) {
        IPage<AccountEntity> addressPage = accountService.queryPage(map);
        return RetUtil.success(addressPage);
    }


}
