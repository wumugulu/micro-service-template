package com.wumugulu.dubbo.web.setting;

import io.swagger.v3.oas.annotations.ExternalDocumentation;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.servers.ServerVariable;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.oas.annotations.EnableOpenApi;

@Configuration
@EnableOpenApi
public class Swagger3Config {

//    @Bean
//    public GroupedOpenApi publicApi() {
//        return GroupedOpenApi.builder()
//                .group("publicApi")
//                .pathsToMatch("/address/**", "/article/**")
//                .build();
//    }
//
//    @Bean
//    public GroupedOpenApi commonApi() {
//        return GroupedOpenApi.builder()
//                .group("commonApi")
//                .pathsToMatch("/authority/**", "/cart/**")
//                .build();
//    }

}
