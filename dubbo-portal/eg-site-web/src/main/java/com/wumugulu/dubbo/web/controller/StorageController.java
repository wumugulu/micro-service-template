package com.wumugulu.dubbo.web.controller;


import cn.hutool.core.lang.Dict;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.wumugulu.dubbo.kernel.bean.RetUtil;
import com.wumugulu.dubbo.basedata.entity.storage.StorageEntity;
import com.wumugulu.dubbo.provider.storage.service.StorageService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 文章 前端控制器
 * </p>
 *
 * @author 黑山老妖
 * @since 2021-05-07
 */
@RestController
@RequestMapping("/storage")
@Slf4j
public class StorageController {

    @DubboReference
    private StorageService storageService;

    @RequestMapping("/getById")
    public Dict getById(Integer id){
        StorageEntity entity = storageService.getById(id);
        return RetUtil.success(entity);
    }

//    @RequestMapping("/queryByWrapper")
//    public List<Map<String, Object>> queryByWrapper(){
//        return storageService.queryByWrapper();
//    }

    /**
     *
     * @param map = {"userId":73189,"saleType":2,"tableId":"001"}
     * @return
     */
    @RequestMapping("/queryList")
    public Dict queryList(@RequestBody Map<String, Object> map) {
        log.info("Controller: {}, {}, {}", "/storage", "/queryList", JSONUtil.toJsonStr(map));
        List<StorageEntity> entityList = storageService.queryList(map);
        return RetUtil.success(entityList);
    }

    /**
     *
     * @param map
     * {"userId":73189,"saleType":2,"tableId":"001","_page":2,"_limit":5}
     *
     * @return
     */
    @RequestMapping("/queryPage")
    public Dict queryPage(@RequestBody Map<String, Object> map) {
        log.info("Controller: {}, {}, {}", "/storage", "/queryPage", JSONUtil.toJsonStr(map));
        IPage<StorageEntity> entityPage = storageService.queryPage(map);
        return RetUtil.success(entityPage);
    }

}
