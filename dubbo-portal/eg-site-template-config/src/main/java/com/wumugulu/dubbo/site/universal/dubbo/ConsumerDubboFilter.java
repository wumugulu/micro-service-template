package com.wumugulu.dubbo.site.universal.dubbo;

import com.wumugulu.dubbo.common.regular.Constant;
import com.wumugulu.dubbo.common.context.UserContext;
import org.apache.dubbo.common.constants.CommonConstants;
import org.apache.dubbo.common.extension.Activate;
import org.apache.dubbo.rpc.Filter;
import org.apache.dubbo.rpc.Invocation;
import org.apache.dubbo.rpc.Invoker;
import org.apache.dubbo.rpc.Result;
import org.apache.dubbo.rpc.RpcContext;
import org.apache.dubbo.rpc.RpcException;

@Activate(group = CommonConstants.CONSUMER)
public class ConsumerDubboFilter implements Filter {

    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        // transfer UserDetail Object to dubbo Provider via RpcContext
        System.err.println("ConsumerDubboFilter: read from consumer's UserContext and Put into dubbo's RpcContext = " + UserContext.getObject());
        RpcContext.getContext().setObjectAttachment(Constant.DUBBO_KEY_USER_DETAIL, UserContext.getObject());

        return invoker.invoke(invocation);
    }

}
