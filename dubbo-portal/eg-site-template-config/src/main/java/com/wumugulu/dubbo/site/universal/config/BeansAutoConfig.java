package com.wumugulu.dubbo.site.universal.config;

import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class BeansAutoConfig {

    @Value("${practice.username:NullValue}")
    private String configUsername;
    @Value("${practice.domain:NullValue}")
    private String configDomain;

    @Bean(name = "PortalConfigBean")
    public Map<String, Object> getGlobalConfig() {
        System.err.println("site/universal/BeansAutoConfig getGlobalConfig init ...... " + configUsername + ": " + configDomain);
        HashMap<String, Object> map = new HashMap<>();
        map.put("username", configUsername);
        map.put("domain", configDomain);
        return map;
    }

    /**
     *
     * 利用 ResponseBodyAdvice 统一输出结果的类型为 Ret 时,默认的convert会出现异常(Ret can't cast to String)
     * 所以,替换之
     *
     */
    @Bean
    public HttpMessageConverters egMessageConverts(){
        return new HttpMessageConverters(new FastJsonHttpMessageConverter());
    }

}
