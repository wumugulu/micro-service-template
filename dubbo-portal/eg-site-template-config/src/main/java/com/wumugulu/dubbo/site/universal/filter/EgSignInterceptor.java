package com.wumugulu.dubbo.site.universal.filter;

import cn.hutool.json.JSONUtil;
import com.wumugulu.dubbo.kernel.bean.RetUtil;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class EgSignInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //return HandlerInterceptor.super.preHandle(request, response, handler);
        System.err.println("site/template/EgSignInterceptor.preHandle is executed ......");
        String actualDomain = request.getParameter("domain");
        /*if (!"ddky".equals(actualDomain)) {
            System.err.println("actualDomain = " + actualDomain + ", wanted: ddky");
            renderJson(response, HttpStatus.NETWORK_AUTHENTICATION_REQUIRED.value());
            return false;
        }*/
        return true;
    }

    private void renderJson(HttpServletResponse response, Integer code) throws Exception{
        PrintWriter writer = null;
        response.setStatus(HttpStatus.OK.value());
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        // response.setContentType("html/text; charset=utf-8");
        try {
            writer = response.getWriter();
            writer.print(JSONUtil.toJsonStr(RetUtil.error(3003, "Sign校验失败")));
        } catch (IOException e) {
            System.err.println("response error: " + e.getMessage());
        } finally {
            if (writer != null)
                writer.close();
        }
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        // HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        // HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }
}
