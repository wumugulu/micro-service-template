package com.wumugulu.dubbo.site.universal.advice;

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.StrUtil;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonInputMessage;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdvice;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;

/**
 *
 * SpringBoot接口加密解密统一处理
 * https://www.zhangshengrong.com/p/q0Xpq2eD1K/
 *
 */
@ControllerAdvice
public class EgRequestBodyAdvice implements RequestBodyAdvice {

    @Override
    public boolean supports(MethodParameter methodParameter, Type type, Class<? extends HttpMessageConverter<?>> aClass) {
//        System.err.println("****** EgRequestBodyAdvice.supports");
//        System.err.println("methodParameter=" + methodParameter);
//        System.err.println("type=" + type);
//        System.err.println("aClass=" + aClass);
        return true;
    }

    @Override
    public HttpInputMessage beforeBodyRead(HttpInputMessage inputMessage, MethodParameter methodParameter, Type type, Class<? extends HttpMessageConverter<?>> aClass) throws IOException {
//        System.err.println("****** EgRequestBodyAdvice.beforeBodyRead");
//        System.err.println("inputMessage=" + inputMessage);
        // return inputMessage;
        // 如果有加密解密的动作: 可以放在这里
        return convertHttpInputMessage(inputMessage);
    }

    @Override
    public Object afterBodyRead(Object body, HttpInputMessage httpInputMessage, MethodParameter methodParameter, Type type, Class<? extends HttpMessageConverter<?>> aClass) {
//        System.err.println("****** EgRequestBodyAdvice.afterBodyRead");
//        System.err.println("body=" + body);
        return body;
    }

    @Override
    public Object handleEmptyBody(Object body, HttpInputMessage httpInputMessage, MethodParameter methodParameter, Type type, Class<? extends HttpMessageConverter<?>> aClass) {
//        System.err.println("****** EgRequestBodyAdvice.handleEmptyBody");
//        System.err.println("body=" + body);
        return body;
    }

    private HttpInputMessage convertHttpInputMessage(HttpInputMessage msg) throws IOException {
        InputStream is = msg.getBody();
        String content = IoUtil.read(is, StandardCharsets.UTF_8);
        String newContent = content.replace("key", "param");

        ByteArrayInputStream inputStream = new ByteArrayInputStream(newContent.getBytes(StandardCharsets.UTF_8));
        MappingJacksonInputMessage newMessage = new MappingJacksonInputMessage(inputStream, msg.getHeaders());
        return newMessage;
    }
}
