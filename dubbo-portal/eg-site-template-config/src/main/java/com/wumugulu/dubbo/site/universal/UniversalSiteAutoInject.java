package com.wumugulu.dubbo.site.universal;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = {"com.wumugulu.dubbo.site.universal"})
public class UniversalSiteAutoInject {
}
