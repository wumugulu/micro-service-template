package com.wumugulu.dubbo.site.universal.advice;

import cn.hutool.core.lang.Dict;
import com.wumugulu.dubbo.kernel.bean.RetUtil;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

@ControllerAdvice
public class EgResponseBodyAdvice implements ResponseBodyAdvice<Object> {

    @Override
    public boolean supports(MethodParameter methodParameter, Class aClass) {
        System.err.println("****** EgResponseBodyAdvice.supports");
        return true;
    }

    @Override
    public Object beforeBodyWrite(Object body, MethodParameter methodParameter, MediaType mediaType, Class aClass, ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {
//        System.err.println("****** EgResponseBodyAdvice.beforeBodyWrite");
//        System.err.println("body=" + body);
//        System.err.println("class=" + body.getClass().getName());
//        System.err.println("methodParameter=" + methodParameter);
//        System.err.println("mediaType=" + mediaType);
//        System.err.println("aClass=" + aClass);
//         return body;
//        if (body instanceof String) {
//            return body + " -- success(this suffix is added by ResponseBodyAdvice!)";
//        }
        // 返回类型已经是Ret,无需处理,直接返回
        if (body instanceof Dict) {
            return body;
        }

        // 将返回的数据封装为Ret类型(注意设置Header)
        if (body instanceof String) {
            body += " -- success(this suffix is added by ResponseBodyAdvice!)";
        }
        serverHttpResponse.getHeaders().add("Content-Type", "application/json");
        return RetUtil.success(body);
    }

}
