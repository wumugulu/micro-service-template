package com.wumugulu.dubbo.site.universal.filter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class EgWebMvcConfig implements WebMvcConfigurer {

    @Value("${spring.profiles.active}")
    private String profile;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        if ("dev".equals(profile)){
            System.err.println("site/template/EgSignInterceptor is added ......");
            registry.addInterceptor(new EgSignInterceptor());
        }
    }
}
