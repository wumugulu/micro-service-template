package com.wumugulu.dubbo.site.universal.filter;

import cn.hutool.core.util.RandomUtil;
import com.wumugulu.dubbo.common.context.UserContext;
import com.wumugulu.dubbo.common.context.UserDetail;
import org.springframework.stereotype.Component;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;
import java.util.Date;

@Component
public class ConsumerServletFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        UserDetail userDetail = new UserDetail();
        userDetail.setId(RandomUtil.randomLong(10000000L, 99999999L));
        userDetail.setUsername(RandomUtil.randomString(10));
        userDetail.setCompany("DDJK - " + RandomUtil.randomNumbers(3));
        userDetail.setLoginTime(new Date());

        System.err.println("ServletFilter generate UserContext ... userDetail's id = " + userDetail.getId());
        UserContext.setObject(userDetail);

        filterChain.doFilter(servletRequest, servletResponse);
    }

}
