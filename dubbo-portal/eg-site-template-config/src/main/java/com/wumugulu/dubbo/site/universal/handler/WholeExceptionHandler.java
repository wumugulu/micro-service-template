package com.wumugulu.dubbo.site.universal.handler;

import cn.hutool.core.lang.Dict;
import com.wumugulu.dubbo.kernel.bean.RetUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
@Slf4j
public class WholeExceptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Dict exceptionHandler(Exception e){
        System.err.println("【Exception系统异常】 —— 异常内容：" + e.getMessage());
        return RetUtil.error(500, "【Exception系统异常】 —— " + e.getMessage());
    }

}
